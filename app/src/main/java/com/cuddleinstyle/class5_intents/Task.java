package com.cuddleinstyle.class5_intents;



/**
 * Created by anmol on 7/9/15.
 */
public class Task {
    CharSequence title;
    CharSequence shortDesc;
    boolean isDone;

    Task(CharSequence title, CharSequence shortDesc, boolean isDone){
        this.title = title;
        this.shortDesc = shortDesc;
        this.isDone = isDone;
    }
}
