package com.cuddleinstyle.class5_intents;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;

public class toDo extends AppCompatActivity implements View.OnClickListener{

    LayoutInflater inflater;
    CustomAdapter adapter;
    ArrayList<Task> data;
    ListView lv;

    public void getbackupdata(){
        try {
            FileInputStream input = openFileInput("lines.txt"); // Open input stream
            DataInputStream din = new DataInputStream(input);
            int sz = din.readInt(); // Read line count
            for (int i = 0; i < sz; i++) { // Read lines
                String backuptitle = din.readUTF();
                String backupdesc = din.readUTF();
                boolean backupdone = din.readBoolean();
                Task temp = new Task(backuptitle,backupdesc,backupdone);
                data.add(temp);
            }
            din.close();
        }catch (IOException e){ e.printStackTrace(); }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        lv = (ListView) findViewById(R.id.lv);
        data = new ArrayList<>();
        getbackupdata();
        inflater = getLayoutInflater();
        adapter = new CustomAdapter(this,data,inflater);
        lv.setAdapter(adapter);

        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent i = new Intent();
                i.setClass(toDo.this, Details.class);
                i.putExtra("position", position);
                i.putExtra("head", adapter.getItem(position).title);
                i.putExtra("body", adapter.getItem(position).shortDesc);
                i.putExtra("on", adapter.getItem(position).isDone);
                startActivityForResult(i, 1);
            }
        });


    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.add) {
            Intent i = new Intent();
            i.setClass(toDo.this, Form.class);
            startActivityForResult(i, 0);
        }else if(id == R.id.call){
            Intent i = new Intent(Intent.ACTION_CALL);
            i.setData(Uri.parse("tel:9818697774"));
            startActivity(i);
        }else if(id == R.id.mail){
            Intent i = new Intent(Intent.ACTION_SENDTO);
            i.setData(Uri.parse("mailto:mahajan.anmol@gmail.com"));
            i.putExtra(Intent.EXTRA_SUBJECT, "Test Email");
            i.putExtra(Intent.EXTRA_TEXT,"The date and time is " + new Date().toString());
            startActivity(i);
        }

        return super.onOptionsItemSelected(item);
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {


        if(intent == null){
            return;
        }else {
            Bundle b = intent.getExtras();
            if (requestCode == 0) {     //Form
                if (resultCode == 0) {
                    CharSequence title;
                    CharSequence message;
                    title = b.getCharSequence("title");
                    message = b.getCharSequence("description");
                    Task temp = new Task(title, message, true);
                    data.add(temp);
                    adapter.notifyDataSetChanged();
                }
            } else if (requestCode == 1) {     //Details
                if (resultCode == 0) {
                    data.get(b.getInt("pos")).isDone = b.getBoolean("finale");
                    data.get(b.getInt("pos")).shortDesc = b.getCharSequence("body");
                    data.get(b.getInt("pos")).title = b.getCharSequence("head");
                    if(b.getBoolean("confirm") == true) {
                        data.remove(b.getInt("pos"));
                    }
                    adapter.notifyDataSetChanged();

                }

            }
        }
    }

    @Override
    protected void onStop() {
        try {
            FileOutputStream output = openFileOutput("lines.txt", MODE_PRIVATE);
            DataOutputStream dout = new DataOutputStream(output);
            dout.writeInt(data.size()); // Save line count
            for(int i = 0; i < data.size(); i++){
                dout.writeUTF(data.get(i).title.toString());
                dout.writeUTF(data.get(i).shortDesc.toString());
                dout.writeBoolean(data.get(i).isDone);
            }
            dout.flush(); // Flush stream ...
            dout.close(); // ... and close.
        }catch (IOException e){ e.printStackTrace(); };
        super.onStop();
    }

    @Override
    public void onClick(View v) {

    }
}
