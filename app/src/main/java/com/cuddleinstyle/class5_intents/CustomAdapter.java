package com.cuddleinstyle.class5_intents;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.TextView;
import java.util.ArrayList;

/**
 * Created by anmol on 7/9/15.
 */
public class CustomAdapter extends ArrayAdapter<Task> {

    LayoutInflater inflater;

    public CustomAdapter(Context context, ArrayList<Task> data, LayoutInflater inflater){
        super(context, 0, data);
        this.inflater = inflater;
    }


    public View getView(int position, View convertView, ViewGroup parent){
            View v = inflater.inflate(R.layout.task_layout,null);
            TextView tv = (TextView) v.findViewById(R.id.mainTextView);
            CheckBox rb = (CheckBox)v.findViewById(R.id.rad);
            tv.setText(getItem(position).title + "");
            rb.setChecked(getItem(position).isDone);
            if(getItem(position).isDone == true){
                tv.setTextColor(Color.parseColor("#000000"));
            }else{
                tv.setTextColor(Color.parseColor("#BBBBBB"));
            }

        return v;
    }



}
