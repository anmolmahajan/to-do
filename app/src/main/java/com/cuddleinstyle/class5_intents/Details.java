package com.cuddleinstyle.class5_intents;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class Details extends AppCompatActivity implements View.OnClickListener {
    Button toggButton;
    Intent j;
    Intent i;
    EditText title;
    EditText body;
    boolean sw,confirm;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);
        j = getIntent();
        i = new Intent();
        confirm = false;
        title = (EditText) findViewById(R.id.dethead);
        body = (EditText) findViewById(R.id.detbody);
        Button delButton = (Button) findViewById(R.id.del);
        toggButton  = (Button) findViewById(R.id.toggle);

        body.setText(j.getExtras().getCharSequence("body"));
        title.setText(j.getExtras().getCharSequence("head"));
        sw = j.getExtras().getBoolean("on");

        toggColor();

        delButton.setOnClickListener(this);
        toggButton.setOnClickListener(this);

        delButton.setTransformationMethod(null);
        toggButton.setTransformationMethod(null);
    }

    public void onBackPressed(){
        i.putExtra("head", title.getText().toString());
        i.putExtra("body", body.getText().toString());
        i.putExtra("finale", sw);
        i.putExtra("pos", j.getExtras().getInt("position"));
        i.putExtra("confirm",confirm);
        setResult(0, i);
        super.onBackPressed();

    }

    protected void toggColor(){
        if(sw == true){
            toggButton.setTextColor(Color.parseColor("#008F39"));
        }
        else {
            toggButton.setTextColor(Color.parseColor("#BBBBBB"));
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_details, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();

        if(id == R.id.del){
            AlertDialog.Builder b = new AlertDialog.Builder(Details.this);
            b.setTitle("Confirm?");
            b.setMessage("Are you sure you want to delete this task? ");

            b.setNegativeButton("Yes", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    confirm = true;
                    onBackPressed();
                }
            });

            b.setPositiveButton("Cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                }
            });
            b.show();
        }
        else if (id == R.id.toggle){
            sw = !sw;
            toggColor();

        }

    }
}
