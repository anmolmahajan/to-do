package com.cuddleinstyle.class5_intents;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class Form extends AppCompatActivity implements View.OnClickListener{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.form_layout);
        Button doneButton = (Button) findViewById(R.id.doneForm);
        Button cancelButton = (Button) findViewById(R.id.cancelForm);

        doneButton.setOnClickListener(this);
        cancelButton.setOnClickListener(this);
        doneButton.setTransformationMethod(null);
        cancelButton.setTransformationMethod(null);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_next, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();

        if(id == R.id.cancelForm){
            finish();
        }
        else if(id == R.id.doneForm){
            Intent i = new Intent();
            EditText _head = (EditText) findViewById(R.id.head);
            EditText _body = (EditText) findViewById(R.id.body);
            CharSequence headstr = _head.getText().toString();
            CharSequence bodystr = _body.getText().toString();
            i.putExtra("title", headstr);
            i.putExtra("description", bodystr);
            if(headstr.length() != 0 && bodystr.length() != 0) {
                setResult(0, i);
                finish();
            }
            else{
                Toast.makeText(Form.this,"Fill all Fields!",Toast.LENGTH_SHORT).show();
            }
        }

    }
}
